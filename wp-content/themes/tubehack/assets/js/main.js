var $ = jQuery.noConflict();
$(window).on('load resize', function () {
    if (window.innerWidth >= 768) {
        $('.hamburger, .close-menu').hide();
        $('.menu-primary-menu-container').show();
    } else {
        $('.hamburger').show();
        $('.close-menu').hide();
        $('.menu-primary-menu-container').hide();
    }
});
$(document).ready(function () {
    $('.hamburger').on('click', function (e) {
        e.stopPropagation();
        // $('.overlay').addClass('active');
        $('.menu-cat-main').addClass('open');
        $('.menu-primary-menu-container').fadeIn();
        $('.close-icon').hide();
        $('.close-menu').show();
        $('.hamburger').hide();
        $('body').addClass('hideExtra');
    });

    $('.close-menu, .overlay').on('click', function (e) {
        e.stopPropagation();
        $('.menu-cat-main').removeClass('open');
        // $('.overlay').removeClass('active');
        $('.menu-primary-menu-container').fadeOut();
        $('.hamburger').show();
        $('.close-menu').hide();
        $('body').removeClass('hideExtra');
    });

    // Slider Initialization homepage banner
    $('.banner__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: false,
        autoplay: true
    });

    $('.screenshots__slider').slick({
        dots: true,
        infinite: true,
        speed: 400,
        slidesToShow: 1,
        accessibility: true,
        adaptiveHeight: true,
        centerMode: true,
        centerPadding: '0px',
        slidesPerRow: 1,
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        arrows: true,
        responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                }
            }
        ]
    });

    // =======================================================================s

    $("input").attr("autocomplete", "off");

    $('.menu-share-links-container li a').attr("target","_blank");
});