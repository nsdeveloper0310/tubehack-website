<?php /* Template Name: Download template */ ?>
<?php
get_header();
?>
<main>
	<?php
		get_template_part('template-parts/download/content','banner');
		// get_template_part('template-parts/download/content', 'lts-version');
		get_template_part('template-parts/download/content', 'versions');
	?>
</main>

<?php
get_footer();
?>