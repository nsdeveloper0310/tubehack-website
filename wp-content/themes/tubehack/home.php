<?php /* Template Name: home page template */ ?>
<?php
get_header();
?>
<main>
	<?php
		get_template_part('template-parts/home/content','banner-section');
		get_template_part('template-parts/home/content','share');
		get_template_part('template-parts/home/content', 'custom');
		get_template_part('template-parts/home/content', 'screenshot');
	?>
</main>

<?php
get_footer();
?>