<footer>
	<div class="footer-wrap">
		<div class="wrapper cf">
			<div class="footer-wrap--about">
				<h6>
				<?php if ( function_exists( 'the_custom_logo' ) ) {
					the_custom_logo();
				} ?> <span>TUBEHACK</span>
				</h6>
				<p class="desc">TubeHack - Youtube video downloader is a free application which allow you to watch and download youtube videos without without annoying ads and questionable permissions.</p>
			</div>
			<div class="footer-wrap--right">
            <?php $bannerSection = get_field('home_banner');?>
				<a href="<?php echo $bannerSection['cta_url'] ?>" class="cta">Get the app now</a>
			</div>
		</div>
	</div>

	<div class="copy-wrap">
		<div class="wrapper">
			<div class="container text-center">
				<p>COPYRIGHT &copy; <?php echo date("Y"); ?> all rights reserved</p>
			</div>
		</div>
	</div>
</footer>
<?php wp_footer();?>
</body>
</html>