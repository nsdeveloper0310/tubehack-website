<?php /* Template Name: About template */ ?>
<?php
get_header();
?>
<main>
	<?php
		get_template_part('template-parts/about/content','about');
	?>
</main>

<?php
get_footer();
?>