<?php $bannerSection = get_field('download_banner_title');?>
<section class="banner download">
    <div class="wrapper">
        <h2><?php echo $bannerSection ?></h2>
    </div>
</section>