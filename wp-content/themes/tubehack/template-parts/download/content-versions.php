<?php $args = array (
	'post_type'              => array( 'version' ),
	'post_status'            => array( 'publish' ),
	'nopaging'               => true,
	'order'                  => 'DSC',
);

// The Query
$versions = new WP_Query( $args );

// The Loop
if ( $versions->have_posts() ) { ?>
    <section class="old-versions">
        <div class="wrapper">
            <h3>All versions</h3>
            <div class="old-versions--wrap">
                <?php while ( $versions->have_posts() ) {
                    $versions->the_post();
                    $versionSingle = get_field('download_versions');?>
                    <div class="version-single">
                        <h4><a href="<?php echo $versionSingle['download_link'] ?>"><?php echo $versionSingle['title'] ?></a></h4>

                        <?php if( count($versionSingle['description']) > 0 ) { ?>
                        <ul class="desc">
                            <?php for($i=0; $i < count($versionSingle['description']); $i++) { ?>
                                <li><span>-</span><?php echo $versionSingle['description'][$i]['desc_points']?></li>
                            <?php } ?>
                        </ul>
                    <?php } ?>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>
<?php } else {
	// no posts found
}