<?php $bannerSection = get_field('latest_stable');?>
<section class="lts-banner">
    <div class="wrapper">
        <div class="lts-banner--wrap">
            <h3><?php echo $bannerSection['title'] ?></h3>
            <?php if( count($bannerSection['description']) > 0 ) { ?>
                <ul class="desc">
                    <?php for($i=0; $i < count($bannerSection['description']); $i++) { ?>
                        <li><?php echo $bannerSection['description'][$i]['desc_details']?></li>
                    <?php } ?>
                </ul>
            <?php } ?>
            <a href="<?php echo $bannerSection['download_link'] ?>" class="cta">Download Here</a>
        </div>
    </div>
</section>