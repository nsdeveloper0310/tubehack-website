<?php $bannerSection = get_field('about_us');?>
<section class="banner download about">
    <div class="wrapper">
        <h2><?php echo $bannerSection['title'] ?></h2>
    </div>
</section>
<section class="about-details">
    <div class="wrapper">
        <h4><?php echo $bannerSection['excerpt'] ?></h4>
        <?php if( count($bannerSection['features']) > 0 ) { ?>
        <?php for($i=0; $i < count($bannerSection['features']); $i++) { ?>
        <div class="single">
            <?php if($bannerSection['features'][$i]['feature_title']) { ?>
                <h5><?php echo $bannerSection['features'][$i]['feature_title']?></h5>
            <?php } ?>
            <?php if($bannerSection['features'][$i]['feature_desc']) { ?>
                <p class="desc"><?php echo $bannerSection['features'][$i]['feature_desc']?></p>
            <?php } ?>
        </div>
        <?php } ?>
        <?php } ?>
    </div>
</section>