<?php $shareTitle = get_field('share_title');

if($shareTitle) { ?>
<section class="share">
    <div class="wrapper">
        <h3><?php echo $shareTitle ?></h3>
        <?php wp_nav_menu( 
			array( 'theme_location' => 'Bottom' ) 
		); ?>
    </div>
</section>
<?php } ?>