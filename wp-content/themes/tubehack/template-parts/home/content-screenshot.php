<?php if( have_rows('screenshots')) { ?>
<section class="screenshots">
    <div class="wrapper">
    <h3>Screenshots</h3>
    <ul class="screenshots__slider">
        <?php while(have_rows('screenshots')){
        the_row(); 
        $image = get_sub_field('ss_image');
        $title = get_sub_field('ss_text'); ?>

        <li class="screenshots__slider--slide">
            <div class="screenshots__slider--slide-right">
                <img src="<?php echo $image ?>" alt="<?php echo $title; ?>">
            </div>
            <?php if($title) { ?>
            <div class="single-content">
                    <h4><?php echo $title ?></h4>
            </div>
            <?php } ?>
        </li>
        <?php } ?>
    </ul>
    </div>
</section>
<?php } ?>