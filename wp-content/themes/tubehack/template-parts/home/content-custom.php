<?php if( have_rows('custom_section')) { ?>
<section class="custom">
    <div class="wrapper">
        <?php while(have_rows('custom_section')){
        the_row(); 
        $image = get_sub_field('image');
        $desc = get_sub_field('desc');
        $title = get_sub_field('title'); ?>

        <div class="custom__single">
            <div class="custom__single--content">
                <h3><?php echo $title ?></h3>
                <p class="desc"><?php echo $desc ?></p>
            </div>
            <div class="custom__single--img">
                <img src="<?php echo $image ?>" alt="<?php echo $title; ?>">
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<?php } ?>