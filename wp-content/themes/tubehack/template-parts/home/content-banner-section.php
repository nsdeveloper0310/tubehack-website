<?php $bannerSection = get_field('home_banner');?>
<section class="banner">
    <div class="wrapper">
        <div class="banner__left">
            <img src="<?php echo $bannerSection['image'] ?>" alt="<?php echo $bannerSection['title']; ?>">
        </div>
        <div class="banner__right">
            <div class="banner__right--logo">
                <img src="<?php echo $bannerSection['logo'] ?>" alt="<?php echo $bannerSection['title']; ?>">
                <span><?php echo $bannerSection['logo_text'] ?></span>
            </div>
            <?php if($bannerSection['title']){ ?>
                <h2><?php echo $bannerSection['title'] ?></h2>
            <?php } ?>
            <p class="banner__right--desc">
                <?php echo $bannerSection['desc'] ?>
            </p>
            <a href="<?php echo $bannerSection['cta_url'] ?>" class="cta"><?php echo $bannerSection['cta_text'] ?></a>
        </div>
    </div>
</section>