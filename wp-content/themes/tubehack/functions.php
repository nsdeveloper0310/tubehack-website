<?php 

// Adding option page
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Footer Settings',
		'menu_slug' 	=> 'theme-general-settings',
        'capability'	=> 'edit_posts',
        'post_id' => 'option',
		'redirect'		=> false
    ));

    // acf_add_options_sub_page(array(
	// 	'page_title' 	=> 'Footer Setting',
	// 	'menu_title'	=> 'Footer',
	// 	'parent_slug'	=> 'theme-general-settings',
	// ));
}

function wpblogs_custom_logo_setup() {
  $defaults = array(
      'height'      => 54,
      'width'       => 306,
      'flex-height' => true,
      'flex-width'  => true,
      'header-text' => array( 'site-title', 'site-description' ),
  );
  add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'wpblogs_custom_logo_setup' );

// Navigation Menu
function register_my_menus() {
    register_nav_menus(
      array(
        'Primary' => __( 'Primary Menu' ),
        'Bottom' => __('Bottom Menu')
        )
    );
  }
  add_action( 'init', 'register_my_menus' );

//Load Theme assets
function theme_assets(){

// for styles
  wp_enqueue_style( 'jqueryuicss', get_template_directory_uri().'/assets/css/jquery-ui.min.css', $deps = array(),'', $media = '' );
  wp_enqueue_style( 'autocompletecss', get_template_directory_uri().'/assets/css/jquery.auto-complete.min.css', $deps = array(), '', $media = '' );
wp_enqueue_style( 'slick-theme', get_template_directory_uri().'/assets/css/slick-theme.css', $deps = array(), filemtime( get_template_directory().'/assets/css/slick-theme.css' ), $media = '' );
wp_enqueue_style( 'font', get_template_directory_uri().'/assets/css/font.css', $deps = array(), filemtime( get_template_directory().'/assets/css/font.css' ), $media = '' );
wp_enqueue_style( 'thm-stylesheet', get_template_directory_uri().'/style.css', $deps = array(), filemtime( get_template_directory().'/style.css' ), $media = '' );

// for Scripts
wp_enqueue_script('jqueryui', get_template_directory_uri().'/assets/js/jquery-ui.js', $deps = array(), filemtime( get_template_directory().'/assets/js/jquery-ui.js' ), $in_footer = true );
wp_enqueue_script('autocomplete', get_template_directory_uri().'/assets/js/jquery.auto-complete.min.js', $deps = array('jquery','jqueryui'), filemtime( get_template_directory().'/assets/js/jquery.auto-complete.min.js' ), $in_footer = true );

wp_enqueue_script( 'slick-min', get_template_directory_uri().'/assets/js/slick.min.js', $deps = array(), filemtime( get_template_directory().'/assets/js/slick.min.js' ), $in_footer = true );
wp_enqueue_script( 'hubslider', get_template_directory_uri().'/assets/js/hubslider.min.js', $deps = array(), filemtime( get_template_directory().'/assets/js/hubslider.min.js' ), $in_footer = true );
wp_enqueue_script( 'thm-main', get_template_directory_uri().'/assets/js/main.js', $deps = array('jquery'), filemtime( get_template_directory().'/assets/js/main.js' ), $in_footer = true );
}
add_action( 'wp_enqueue_scripts', 'theme_assets' );


function favicon() {
	echo '<link rel="Shortcut Icon" type="image/x-icon" href="'.get_template_directory_uri().'/assets/img/favicon.png" />';
}
add_action('wp_head', 'favicon');

// Frontend Admin Bar

show_admin_bar(false);

// Widget sidebars 

function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

	//symptoms analyzer tags pull
  function get_tags_in_use($category_ID, $type = 'name'){
    // Set up the query for our posts
    // $autocat = get_category($args);
    $shw_autocat_id = $autocat->term_id;
    $my_posts = new WP_Query(array(
      'cat' => $shw_autocat_id, // Your category id
      'posts_per_page' => -1 // All posts from that category
    ));
  
    // Initialize our tag arrays
    $tags_by_id = array();
    $tags_by_name = array();
    $tags_by_slug = array();
  
    // If there are posts in this category, loop through them
    if ($my_posts->have_posts()): while ($my_posts->have_posts()): $my_posts->the_post();
  
      // Get all tags of current post
      $post_tags = wp_get_post_tags($my_posts->post->ID);
  
      // Loop through each tag
      foreach ($post_tags as $tag):
  
      // Set up our tags by id, name, and/or slug
        $tag_id = $tag->term_id;
        $tag_name = $tag->name;
        $tag_slug = $tag->slug;
  
      // Push each tag into our main array if not already in it
        if (!in_array($tag_id, $tags_by_id))
          array_push($tags_by_id, $tag_id);
  
        if (!in_array($tag_name, $tags_by_name))
          array_push($tags_by_name, $tag_name);
  
        if (!in_array($tag_slug, $tags_by_slug))
          array_push($tags_by_slug, $tag_slug);
  
      endforeach;
    endwhile; endif;
  
    // Return value specified
    if ($type == 'id')
      return $tags_by_id;
  
    if ($type == 'name')
      return $tags_by_name;
  
    if ($type == 'slug')
      return $tags_by_slug;
  }
  
  function tag_cloud_by_category($category_ID){
    // Get our tag array
    $tags = get_tags_in_use($category_ID, 'id');
  }
  
  // Our custom post type function
function create_posttype() {
    register_post_type('version',
        // CPT Options
        array(
            'labels' => array(
                'name' => __('Versions'),
                'singular_name' => __('version'),
            ),
            'public' => true,
            'has_archive' => true,
            'show_in_menu' => true,
            'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'custom-fields'),
        )
    );
}
// Hooking up our function to theme setup
add_action('init', 'create_posttype');
