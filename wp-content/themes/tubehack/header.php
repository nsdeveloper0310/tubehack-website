<!DOCTYPE html>
<html>
<head>
	<title><?php wp_title('&raquo;', 'true', 'right');?><?php bloginfo('name');?> | <?php bloginfo('description');?></title>
	<meta charset="<?php bloginfo('charset');?>">
	<meta name="description" content="TubeHack - Youtube video downloader is a free application which allow you to watch and download youtube videos without without annoying ads and questionable permissions.">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap" rel="stylesheet">
	<?php wp_head();?>
<!--[if !IE]><!-->
<script>
  if (/*@cc_on!@*/false && document.documentMode === 10) {
    document.documentElement.className+=' ie10';
  }
</script>
<!--<![endif]-->

</head>
<body <?php body_class();?>>
<header class="main-header">
	<div class="wrapper">
		<h1 class="header-logo">
			<?php if ( function_exists( 'the_custom_logo' ) ) {
				the_custom_logo();
			} ?>
		</h1>

		<div class="header-right">
			<div class="menu-cat">
				<div class="hamburger hide">
					<img src="<?php bloginfo('template_url');?>/assets/img/hamb.svg" alt="TubeHack" />
				</div>
				<div class="close-menu hide">
					<img src="<?php bloginfo('template_url');?>/assets/img/close.svg" alt="TubeHack" />
				</div>
				<?php wp_nav_menu( 
					array( 'theme_location' => 'Primary' ) 
				); 
				?>
			</div>
		</div>
	</div>
</header>
<div class="overlay"></div>