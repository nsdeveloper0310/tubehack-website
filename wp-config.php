<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'tubehack' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'RlblV9WN2pSwn0ticSmDY}R/1K:wGIrm7,sJR+G>pr55~>&[}o}.lH&emuj1R@sv' );
define( 'SECURE_AUTH_KEY',  '<X)|wNrcP)nuS7Mz~c2$%@%k~pX|zKs;({x#dxot^kQG$so*{bv}2{Y!jQtoONl5' );
define( 'LOGGED_IN_KEY',    '=(,;QBNFvhf?7kZ?YaR&YlwxDun}MAoPoaaU}S(7;M~[@rVzg`S+jfLU,QZp!eo5' );
define( 'NONCE_KEY',        'vWy1AVib=:`U7BNSbuu%Gd;T/GMKuRPSy07M/YvS`UJcEG1?46[:n^af[_R<K&()' );
define( 'AUTH_SALT',        ';kX=K.z8$ltr-2wH6(^gJk~-Va&kz&:t3+)d[va*WXV3#|4QR/-VH(JId+3{ueo^' );
define( 'SECURE_AUTH_SALT', 'ynmPjtp0s?nFu(Z&yJ>i~?%3+ADZCmNSC[OpMTerK!*?+5BJhgE}C06wu`xygHBr' );
define( 'LOGGED_IN_SALT',   't(9uUnqfPir$JFCNGVX9z5nhQy([H)5<?D8&n-UZ~a;}R9E_-85QR0H4Qg=rNgyI' );
define( 'NONCE_SALT',       ':~(r0d&K-f{On<d]37p!1vK/[#$@AWwRsme]t6CBF5K>8E.>TYLJ?/g6?d1)m3q8' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
